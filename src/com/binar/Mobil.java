package com.binar;

public abstract class Mobil {
    private int jmlBan;
    private int jmlPenumpang;

    abstract String jnsMesin(String merkMobil);
    abstract String wrnMobil(String warna);

    public void kecepatanMobil() {
        String mobil = "mobil";
        System.out.println("Mobil berjalan maksimal 120kmh");
    }

    public int getJmlBan() {
        return jmlBan;
    }

    public void setJmlBan(int jmlBan) {
        this.jmlBan = jmlBan;
    }

    public int getJmlPenumpang() {
        return jmlPenumpang;
    }

    public void setJmlPenumpang(int jmlPenumpang) {
        this.jmlPenumpang = jmlPenumpang;
    }
}
