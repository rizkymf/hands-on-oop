package com.binar.config;

public class ConfigTest {
    public void coba() {
        System.out.println("Ini pake public");
    }

    public void callProtected() {
        ConfigProtected configProtected = new ConfigProtected();
        configProtected.cobaProtect();
    }

    public void callPrivate() {
        ConfigPrivate configPrivate = new ConfigPrivate();
//        configPrivate.cobaPrivate();
        configPrivate.methodDefault();
    }
}
