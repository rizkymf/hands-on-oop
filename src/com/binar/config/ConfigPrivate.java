package com.binar.config;

public class ConfigPrivate {

    private String nama;
    private String noHp;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public void gantiPassword() {

    }

    private void cobaPrivate() {
        System.out.println("ini private");
    }

    void methodDefault() {
        cobaPrivate();
    }
}
