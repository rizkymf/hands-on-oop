package com.binar;

public enum Season {
    WINTER(5, "December"), SPRING(10, "January"),
    SUMMER(15, "February"), FALL(20, "March");

    private int value;
    private String when;
    Season(int value, String when) {
        this.value = value;
        this.when = when;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }
}
