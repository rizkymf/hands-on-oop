package com.binar;

public class HondaBrio extends Mobil {

    @Override
    String jnsMesin(String merkMobil) {
        return "Mesin " + merkMobil;
    }

    @Override
    String wrnMobil(String warna) {
        return "Warna " + warna;
    }
}
