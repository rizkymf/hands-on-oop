package com.binar;

public class Outer {
    public void buatOverride() {
        System.out.println("buat di override");
    }

    public final void overrideFinal() {
        System.out.println("jangan di edit!");
    }

    static class Inner1 {
        public void methodInner1() {
            System.out.println("print methodInner1");
        }

        public static void methodInner12(){
            System.out.println("print methodInner12");
        }
    }

    class Inner2 {
        public void methodInner2() {
            System.out.println("print methodInner2");
        }

        public void methodInner22() {

        }
    }
}
