package com.binar;

import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

import java.util.Locale;

//@Getter
//@Setter
public class Nasabah {
    private String namaNasabah; // Field
    private Integer noHp;


    // Constructor kosong
    Nasabah() {
        System.out.println("Ini constructor kosongan");
    }

    // Constructor ber-parameter
    Nasabah(String namaNasabah) {
//        this.namaNasabah = namaNasabah;
        this.namaNasabah = namaNasabah.toUpperCase(Locale.ROOT);
        System.out.println("Ini constructor berisi, " + namaNasabah);
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public Integer getNoHp() {
        return noHp;
    }

    public void setNoHp(Integer noHp) {
        this.noHp = noHp;
    }

    public void printHalo() {
        System.out.println("Halo saya method printHalo, " + this.namaNasabah);
    }

    public void gantiData(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public void gantiData(Integer noHp) {
        this.noHp = noHp;
    }

    public void gantiData(String namaNasabah, Integer noHp) {

    }

    public void gantiData(Integer noHp, String namaNasabah){

    }

    public void gantiData(String namaNasabah, String alamat) {

    }

    public void callMethod() {
//        gantiData();
        gantiData("Rizky", 12345);
    }
}
